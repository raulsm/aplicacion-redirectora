#!/usr/bin/python3

import socket
import random

# Define the HTTP status code to be used for redirection
redirection_code = 307

# Dictionary containing HTTP status codes and their corresponding messages
redirections = {
    307: "Temporary Redirect"
}

# List of URLs for redirection
redirection_urls = [
    "https://www.urjc.es/",
    "https://www.google.com",
    "https://www.youtube.com",
    "https://www.wikipedia.org",
    "https://cursosweb.github.io/programas/IT-ST.pdf"
]

# Define the port to listen on
myPort = 1234

if __name__ == '__main__':


    # Create a socket object and set it to reuse the address
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    # Bind the socket to the port
    mySocket.bind(('', myPort))

    # Listen for incoming connections with a maximum queue size of 5
    mySocket.listen(5)

    # Initialize the random number generator
    random.seed()


    while True:
        # Wait for incoming connections
        print("Esperando alguna conexión...")
        (recvSocket, addr) = mySocket.accept()

        # Print the received HTTP request
        print("Conexion recibida de:" + str(addr))
        print(recvSocket.recv(2048))

        # Choose a random URL from the redirection_urls list
        nextUrl = random.choice(redirection_urls)

        # HTML body of the page to serve
        htmlBody = "<h1>URL generator</h1>" + '<p>Next url: <a href="' \
                   + nextUrl + '">' + nextUrl + "</a></p>"

        # Construct the HTTP response with the chosen status code, message, and HTML body
        response = b"HTTP/1.1 " + str(redirection_code).encode() + b" " + redirections[
            redirection_code].encode() + b"\r\n\r\n" + \
                   b"<html><body>" + htmlBody.encode('ascii') + b"</body></html>" + b"\r\n"

        # Send the response back to the client
        recvSocket.send(response)

        # Close the connection
        recvSocket.close()



